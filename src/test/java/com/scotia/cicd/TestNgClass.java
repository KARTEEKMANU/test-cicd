package com.scotia.cicd;

import org.testng.annotations.Test;

public class TestNgClass {

    @Test(priority = 1)
    public void m1() {

        System.out.println("This is m1 method");
    }

    @Test(priority = 2)
    public void m2() {

        System.out.println("This is M2 method");
    }
}
